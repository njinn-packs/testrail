import re
from urllib.parse import urlparse

from requests import Session
from requests.auth import HTTPBasicAuth

from .testrail import APIClient


class TestrailBase():

    def __init__(self):
        self.connection = None
        self.__testrail_instance = None

    @property
    def testrail(self):
        if not self.__testrail_instance:
            self.__testrail_instance = self.testrail_instance()
        return self.__testrail_instance

    def testrail_instance(self):
        if self.connection and self.connection.get('url')\
                and self.connection.get('user') \
                and self.connection.get('password'):

            self.url = self.connection.get('url')

            self.parsed_uri = urlparse(self.url)

            testrail = APIClient(self.url)
            testrail.user = self.connection.get('user')
            testrail.password = self.connection.get('password')
        else:
            raise Exception('No or incomplete connection information provided')

        return testrail


class RequestReport(TestrailBase):
    '''
    Requests reports to be generated
    '''

    report_template_id = None

    def run(self):
        if not self.report_template_id:
            raise Exception("Report template ID is required")

        print(f"Requesting report {self.report_template_id}")
        report_render = self.testrail.send_get(
            f"run_report/{self.report_template_id}")

        if not report_render.get('report_pdf'):
            raise Exception(report_render)
        else:
            report_url = report_render.get('report_pdf')
            print(report_render)
            return {'report_urls': report_render}


class RetrievePDFReport(TestrailBase):
    '''
    Retrieve PDF report
    '''

    report_pdf_url = None
    file_name = None

    def run(self):
        if not self.report_pdf_url:
            raise Exception("Report PDF url is required")

        if not self.file_name:
            file_name = f"{self.report_pdf_url.split('/')[-1]}.pdf"
        else:
            if re.findall(r'[^A-Za-z0-9_\-\.\ ]', self.file_name):
                raise Exception(
                    f"Invalid file name {self.file_name} provided, must only contain [A-Za-z0-9_-. ]")
            file_name = self.file_name

        with Session() as s:
            form_data = {'name': self.connection['user'],
                         'password': self.connection['password']}

            print(f'Retrieving PDF {self.report_pdf_url}')
            res_login = s.post(
                f"{self.connection['url']}/index.php?/auth/login", form_data)
            pdf = s.get(self.report_pdf_url)
            if not pdf.ok:
                print("Report not available (probably not generated yet)")
                pdf.raise_for_status()

            with open(file_name, "wb") as pdf_file:
                pdf_file.write(pdf.content)
                pdf_file.close()

            return {'file': self._njinn.upload_file(file_name)}
